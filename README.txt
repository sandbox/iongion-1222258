This module allows RPC between AMF aware clients,
such as :
  - Flash
  - Flex
  - AIR applications
  - others(silverlight using fluorinefx)
and Drupal exposed services.
 
Important notice

This module is greatly inspired by existing AMFPHP module but high expands on it